package processor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

public class JpgToPngProcessor extends SubmissionPublisher<File> implements Flow.Processor<Path, File> {
    private Flow.Subscription subscription;
    private File png;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(Path item) {
        System.out.println("Get file in process in thread: " + Thread.currentThread().getName());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(item.toFile());
            String fileName = item.getFileName().toString()
                    .substring(0, item.getFileName().toString().lastIndexOf('.')) + ".png";
            png = new File(fileName);
            ImageIO.write(bufferedImage, "png", png);
            System.out.println("Convert file: " + item.getFileName().toString());
            submit(png);
        } catch (IOException e) {
            onError(e);
        }
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {

        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Processor is done");
        close();
    }
}
