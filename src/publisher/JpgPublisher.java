package publisher;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SubmissionPublisher;

public class JpgPublisher extends SubmissionPublisher<Path> {
    private static PathMatcher matcher = FileSystems.getDefault()
            .getPathMatcher("glob:**.{jpg,jpeg}");
    private List<Path> listPictures = new LinkedList<>();

//    public JpgPublisher(){
//        super(Executors.newFixedThreadPool(4), 4);
//    }

    public void searchJpg(Path path){
        if(Files.exists(path, LinkOption.NOFOLLOW_LINKS)){
            try {
                Files.walkFileTree(path, new JpgVisitor());
            } catch (IOException e) {
                e.printStackTrace();
            }
            listPictures.forEach(this::submit);
            close();
        }
    }

    @Override
    public void close() {
        super.close();
    }

    private class JpgVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
            if(matcher.matches(path)){
                System.out.println("Found file: " + path.getFileName().toString()
                        + ", submitting in thread: " + Thread.currentThread().getName());
                listPictures.add(path);
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
