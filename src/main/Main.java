package main;

import processor.JpgToPngProcessor;
import publisher.JpgPublisher;
import subscriber.PngSubscriber;

import java.nio.file.Paths;

public class Main {
    private static JpgPublisher jpgPublisher = new JpgPublisher();
    private static JpgToPngProcessor converter = new JpgToPngProcessor();
    private static PngSubscriber saver = new PngSubscriber(Paths.get("D:\\test\\png\\"));
    public static void main(String[] args) {
        jpgPublisher.subscribe(converter);
        converter.subscribe(saver);
        jpgPublisher.searchJpg(Paths.get("D:\\pictures\\"));
    }
}
