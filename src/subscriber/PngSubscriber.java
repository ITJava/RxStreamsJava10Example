package subscriber;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.Flow;

public class PngSubscriber implements Flow.Subscriber<File> {
    private Flow.Subscription subscription;
    private Path path;

    public PngSubscriber(Path pathToSave){
        path = pathToSave;
        if(!Files.isExecutable(path)){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                onError(e);
            }
        }
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(File item) {
        try {
            Files.move(item.toPath(), path.resolve(item.toPath()), StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Save file: " + item.getName());
        } catch (IOException e) {
            onError(e);
        }
        subscription.request(1);
    }

    @Override
    public void onError(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Final subscriber is done");
        subscription.cancel();
    }
}
